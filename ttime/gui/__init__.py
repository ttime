import functools
from ttime.localize import _
import gtk
from ProgressDialog import ProgressDialog
from MainWindow import MainWindow

def warning(error_string):
    error_dlg = gtk.MessageDialog(
            type=gtk.MESSAGE_WARNING,
            buttons=gtk.BUTTONS_OK)
    error_dlg.set_markup(error_string)

    error_dlg.run()
    error_dlg.destroy()

def wrap_error(f):
    @functools.wraps(f)
    def wrapped_f(*args, **kargs):
        try: f(*args, **kargs)
        except Exception, e:
            error(str(e))

    return wrapped_f

def error(error_string):
    error_dlg = gtk.MessageDialog(
            type=gtk.MESSAGE_ERROR,
            buttons=gtk.BUTTONS_OK)
    error_dlg.set_markup(error_string)

    error_dlg.run()
    error_dlg.destroy()
    gtk.main_quit()
